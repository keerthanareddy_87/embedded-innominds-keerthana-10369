/*---------------------------------------------------------------------------------------------------------------------------------*/
/*    BATCH:              NAME:                 EMPLOYEE-ID:           PHONE_NUMBER:                          GMAIL-ID:

      GROUP-B          KEERTHANA REDDY          10369                   9542270284                      keerthanareddy8765@gmail.com
   PURPOSE:
 ---------
In this program we created Single linked list and adding the data at particular location and deleting the data at particular location 
and displaying the linked list elements in the order .


--------------------------------------------------------------------------------------------------------------------------------------*/
#include<iostream>
#include<stdio.h>
#include<stdlib.h>

using namespace std;
/*This class contain structure with data member and self referential structure to point to the next data in the list ,and member function to add and delete the data at particular position*/
class SingleLinkedList {
	int no_of_records;
	struct List {
		string name;
		struct List *next;
	}*head;
	public://Default constructor
	SingleLinkedList(string name) {
		head=new struct List;
		head->name=name;
		head->next=NULL;
		no_of_records=1;
	}
	//Add the data in particular position
	void addAtPosition(string name,int position) {

		struct List *new_record=new struct List;
		new_record->name=name;
		if (position <0 || position > no_of_records+1) {
			cout<< "cannot add the record at position:"<<position<<endl;
		}
		else if(position==0) {
			new_record->next=head;
			head=new_record;
			no_of_records++;
			return;
		}
		else if(position==no_of_records+1) {
			struct List *temp;
			if(head==NULL) {
				head=new_record;
				no_of_records++;
				return;
			}
			for(temp=head;temp->next!=NULL;temp=temp->next);
			temp->next=new_record;
			new_record->next=NULL;
			no_of_records++;
			return;
		}
		else {
			int index;
			struct List *current_record,*previous_record;
			current_record=head;
			for(index=1;index!=position+1;index++) {
				previous_record=current_record;
				current_record=current_record->next;
			}
			previous_record->next=new_record;
			new_record->next=current_record;
			no_of_records++;
			return;
		}
	}

	//Display the data in the linked list
	void displayRecords() {
		if(head==NULL) {
			cout<<"List is empty."<<endl;
			return;
		}
		struct List *temp;
		cout<<"no.of records:"<<no_of_records<<endl;
		cout<<"\t**********LIST*********"<<endl;
		for(temp = head;temp!=NULL;temp = temp->next) {
			cout<<"\t\t"<<temp->name<<endl;
		}
		delete temp;
		return;
	}
	//this method updates the data in which is present in specific position in the list
	void updateByPosition(int position) {
                if(position<=0 || position > no_of_records) {
                        cout<<"cannot update the data at that position:"<<position<<endl;

                }
                int index;
                 struct List *temp=head;
                for(index=1;index<position;index++) {
                        temp=temp->next;
                }
                cout<<"enter the data to be update name:"<<endl;
                cin>>temp->name;
        }
	//delete the data at particular position in the linked list
	void deleteAtPosition(int position) {

		if (position <=0 || position > no_of_records) {
			cout<< "cannot delete the record at that position"<<position<<endl;
		}
		else if(position==1) {
	
			struct List *temp=head;
			head=head->next;
			delete temp;
			temp=NULL;
			no_of_records--;
			return;
		}
		else if(position==no_of_records) {
			struct List *temp,*previous;
			for(temp=head;temp->next!=NULL;previous=temp,temp=temp->next);
			previous->next=NULL;
			delete temp;
			temp=NULL;
			no_of_records--;
			return;
		}
		else {
			int index;
			struct List *current_record,*previous_record;
			current_record=head;
			for(index=1;index!=position;index++) {
				previous_record=current_record;
				current_record=current_record->next;
			}
			previous_record->next=current_record->next;
			delete current_record;
			current_record=NULL;
			no_of_records--;
			return;
		}
	}
	//Default constructor
	~SingleLinkedList() {
		delete head;
	}
};

int main(void) {
	SingleLinkedList singlelinkedlist("keerthana");
	string name;
	int choice;
	int position;
	while(1) {
		cout<<"*****************MENU*****************"<<endl;
		cout<<"1.Add record at given position"<<endl;
		cout<<"2.Delete at given position"<<endl;
		cout<<"3.update by position"<<endl;
		cout<<"4.Display all records"<<endl;
		cout<<"5.exit"<<endl;
		cout<<"enter your choice:"<<endl;
		cin>>choice;
		switch(choice) {
			case 1:
					cout<<"enter name to be add:"<<endl;
					cin>>name;
					cout<<"enter the position at which u want to add:"<<endl;
					cin>>position;
					singlelinkedlist.addAtPosition(name,position);
				break;
			case 2:
					cout<<"enter the position:"<<endl;
					cin>>position;
					singlelinkedlist.deleteAtPosition(position);
				break;
			case 3:
					cout<<"enter the position:"<<endl;
					cin>>position;
					singlelinkedlist.updateByPosition(position);
				break;
			case 4:
				singlelinkedlist.displayRecords();
				break;
			case 5:
				exit(0);
			default: 
				cout<<"choice is in between 1 and 4"<<endl;

		}
	} 
}




/*************************************************************************************


	OUTPUT:
-------------------------


*****************MENU*****************
1.Add record at given position
2.Delete at given position
3.update by position
4.Display all records
5.exit
enter your choice:
1
enter name to be add:
keerthana
enter the position at which u want to add:
1
*****************MENU*****************
1.Add record at given position
2.Delete at given position
3.update by position
4.Display all records
5.exit
enter your choice:
1
enter name to be add:
bhavani
enter the position at which u want to add:
1
*****************MENU*****************
1.Add record at given position
2.Delete at given position
3.update by position
4.Display all records
5.exit
enter your choice:
4
no.of records:3
	**********LIST*********
		keerthana
		bhavani
		keerthana
*****************MENU*****************
1.Add record at given position
2.Delete at given position
3.update by position
4.Display all records
5.exit
enter your choice:
3
enter the position:
1
enter the data to be update name:
om
*****************MENU*****************
1.Add record at given position
2.Delete at given position
3.update by position
4.Display all records
5.exit
enter your choice:
4
no.of records:3
	**********LIST*********
		om
		bhavani
		keerthana



**************************************************************************/
