/*---------------------------------------------------------------------------------------------------------------------------------*/
/*              NAME:                 EMPLOYEE-ID:           PHONE_NUMBER:                          GMAIL-ID:

            KEERTHANA REDDY          10369                   9542270284                      keerthanareddy8765@gmail.com
   PURPOSE:
 ---------



------------------------------------------------------------------------------------------------------------------------------------*/
#include<iostream>
using namespace std;

class InsertionSort{

	int *array;
	int size;
	public:
		InsertionSort(){
			cout<<"enter the size of array"<<endl;
			cin>>size;
			array = new int[size];
			cout<<"enter array elements:"<<endl;
			for(int i=0;i<size;i++)
			cin>>array[i];
			}
			
		void insertionSort();
		void displayArrayElements();
		~InsertionSort(){
		}
};


void InsertionSort :: insertionSort() {
	int i,j,temp;
	for(int i=1;i<size;i++)
	{
		int j=i-1;
		temp=array[i];
		while(j>=0 && array[j]>temp) {
		array[j+1]=array[j];
		j--;
		}
		array[j+1]=temp;
	}
}

	
void InsertionSort :: displayArrayElements() {
	for(int i=0;i<size;i++){
	cout<<array[i]<<" ";
	}
	cout<<endl;
}


int main()
{
	InsertionSort insertionsort;
	cout<<"array elements before sorting:"<<endl;
	insertionsort.displayArrayElements();
	insertionsort.insertionSort();
	cout<<"array elements after sorting:"<<endl;
	insertionsort.displayArrayElements();
	return 0;
}


/*----------------------------------------------------------------------------------------------------------------


OUTPUT:
---------------------------
enter the size of array
8
enter array elements:
9
3
1
7
3
6
2
8
array elements before sorting:
9 3 1 7 3 6 2 8 
array elements after sorting:
1 2 3 3 6 7 8 9 
-------------------------------------------------------------------------------------------------------------------*/
