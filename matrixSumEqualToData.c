/**************************************************************************************************************************************
 	NAME					EMAIL-ID		PHONE NUMBER         EMPLOYEE-ID
---------------------------------------------------------------------------------------------------------------------------------------	
    G.KEERTHANAREDDY			keerthanareddy8765@gmail.com	  9542270284            10369
---------------------------------------------------------------------------------------------------------------------------------------

This program is an implementaion of finding whose sum of the two values is equals to the given data in the array and storing the values resultant array and displaying the array. 
**************************************************************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
//defining the size of array
#define SIZE 5

//function to find the sum of values equal to the given data
int* getArray(int* arr,int* arr1,int data){
	int i,j;
	//allocating memory dynamically  
	arr1=(int*)malloc(2*(sizeof(int)));
	for(i=0,j=SIZE-1;i<j;){
		//checking the conditions if the sum of the two values in the array 
		//is equal to the given data or not.
		if((arr[i]+arr[j])==data){
				arr1[0]=i;
				arr1[1]=j;
				return arr1;
			//if sum is greater than given value then decrease the valu of j
			}else if((arr[i]+arr[j])>data){
				j--;
			}//else increment the value of i
			else{
				i++;
		}
	}
	return NULL;
}

int main(){
	int arr[SIZE]={6,17,15,55,70};
	int *arr1,data;
	printf("Enter data \n");
	scanf("%d",&data);
	arr1=getArray(arr,arr1,data);
	//checking the conditions if the returned array is equal to null or not 
	// if yes then print sum is  not found statement otherwise print indexes of the pair values 
	if(arr1==NULL){
		printf("Sum of the values for %d is not found\n",data);
	}else{
		printf("values present in the indexes i=%d j=%d is equal to the given data %d\n",arr1[0],arr1[1],data);
	}
	return 0;
}
/************************************************OUTPUT OF THE ABOVE PROGRAM*****************************************************
Enter data 
72
values present in the indexes i=1 j=3 is equal to the given data 72

**************************************************************************************************************************************/ 

