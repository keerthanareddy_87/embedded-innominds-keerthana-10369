/**************************************************************************************************
NAME                      EMAILID                            PHONE                   EMPID
------------------------------------------------------------------------------------------------------------
G KEERTHANA REDDY        keerthanareddy8765@gmail.com     9542270284                10369

PURPOSE:
----------------
This program gives the output along with content present in source code. 
***********************************************************************************************/
#include <stdio.h>
#include <stdlib.h>

int main(void) {

	FILE *fp = NULL;
	int ch;
	fp = fopen(__FILE__,"r");	
	//This give the file name which is we were given.
	//printf("%s\n",__FILE__);
	if(fp == NULL) {
		perror("fp");
		return -1;
	}
	printf("FILE OPENED SUCCESSFULLY\n");
	printf("MY NAME IS KEERTHANA\n");
	printf("------------------------------------------------\n");
	while((ch=fgetc(fp)) != -1) {
		printf("%c",ch);
	}
	fclose(fp);
	return 0;
}
/***************************OUTPUT******************************
#include <stdio.h>
#include <stdlib.h>

int main(void) {

	FILE *fp = NULL;
	int ch;
	fp = fopen(__FILE__,"r");	
	//This give the file name which is we were given.
	//printf("%s\n",__FILE__);
	if(fp == NULL) {
		perror("fp");
		return -1;
	}
	printf("FILE OPENED SUCCESSFULLY\n");
	printf("MY NAME IS KEERTHANA\n");
	printf("------------------------------------------------\n");
	while((ch=fgetc(fp)) != -1) {
		printf("%c",ch);
	}
	fclose(fp);
	return 0;
}

 ****************************************************************/
