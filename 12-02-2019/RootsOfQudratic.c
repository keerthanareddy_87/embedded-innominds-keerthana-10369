/*---------------------------------------------------------------------------------------------------
NAME                      EMAILID                            PHONE                   EMPID
------------------------------------------------------------------------------------------------------------
G KEERTHANA REDDY        keerthanareddy8765@gmail.com     9542270284                10369

PURPOSE:
----------------
This program deals with finding the roots for a given 2 degreee quadratic equation. 
------------------------------------------------------------------------------------------------------*/

#include<stdio.h>
#include<math.h>
int main() {
	int a,b,c;
	float root1,root2;
	printf("enter the coefficients of qudratic equation \n");
	scanf("%d %d %d",&a,&b,&c);
	while(a==0) {
		printf("highest coefficient cannot be zero....please renter \n");
		scanf("%d",&a);
	}
	float r=(b*b)-4*a*c;
	if(r<0) {
		r=-r;
		root1=(float)-b/(2*a);
		printf("%f\n",root1);
		root2=sqrt(r)/(2*a);
		printf("the 1st complex  root of equation is %f+i%f\n",root1,root2);
		printf("the 2nd complex root of equation is %f-i%f\n",root1,root2);
	} else {


		root1=(-b+sqrt(r))/(2*a);
		root2=(-b-sqrt(r))/(2*a);
		printf("the 1st root of equation is %f\n",root1);
		printf("the 2nd root of equation is %f\n",root2);
	}
	return 0;
}

/*********************OUTPUT****************************************************
enter the coefficients of qudratic equation 
4
5
6
-0.625000
the 1st complex  root of equation is -0.625000+i1.053269
the 2nd complex root of equation is -0.625000-i1.053269
---------------------------------------------------------------------
enter the coefficients of qudratic equation 
2
0
5
0.000000
the 1st complex  root of equation is 0.000000+i1.581139
the 2nd complex root of equation is 0.000000-i1.581139
-------------------------------------------------------------
enter the coefficients of qudratic equation 
0
4
7
highest coefficient cannot be zero....please renter 

 
**********************************************************************/

