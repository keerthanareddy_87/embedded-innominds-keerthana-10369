#include"headers.h"

int main(){
	//declaring the variables
	int choice;
	int data1,data2,resultantData;
	int *ptr = &resultantData;
	//displaying the menu for selecting the options
	while(1){
	printf("*****MENU FOR SELECTING THE OPERATIONS IN THE CALCULATOR*****\n1.Addition\n2.Subtraction\n3.Multiplication\n4.Division\n5.EXIT\n");
	printf("Enter the choice\n");
	scanf("%d",&choice);
	switch(choice){
			//case for calling addition function
		case 1:printf("Enter two integer values for performing addition operation\n");
		       scanf("%d %d",&data1,&data2);
		       additionOfTwoNumbers(data1,data2,ptr);
		       printf("Sum of two numbers is %d\n",resultantData);
		       break;
			//case for calling subtraction function
		case 2:printf("Enter two integer values for performing subtraction operation\n");
		       scanf("%d %d",&data1,&data2);
		       subtractionOfTwoNumbers(data1,data2,ptr); 
	               printf("difference of two numbers is %d\n",resultantData);
		       break;
			//case for calling multiplication function
		case 3:printf("Enter two integer values for performing multiplication operation\n");
		       scanf("%d %d",&data1,&data2);
		       multiplicationOfTwoNumbers(data1,data2,ptr);
	               printf("Product of two numbers is %d\n",resultantData);
		       break;
			//case for calling division function
		case 4:printf("Enter numerator\n");
		       scanf("%d",&data1);
		       int flag=0;
		       while(flag==0){
			       printf("enter denominator\n");
			       scanf("%d",&data2);
			       if(data2>0){
		      		 divisionOfTwoNumbers(data1,data2,ptr);
		       		 printf("Quotient of two numbers is %d\n",resultantData);
				 flag=1;
				 }else{
			      		 printf("Division is not possible for denominator values less than or equal to zero\n");
				}
		       }
		       break;
		case 5:exit(0);
		default:printf("INVALID CHOICE\n");
	}
	}
	return 0;

	}
