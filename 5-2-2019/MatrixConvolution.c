/************************************************************************************************************
    NAME 		MOBILE NUMBER 			EMAIL-ID 			EMPLOYEE-ID
------------------------------------------------------------------------------------------------------------
G.KEERTHANAREDDY        9542270284                      keerthanareddy8765@gmail.com     10369
-----------------------------------------------------------------------------------------------------------
This program is the implementation of convolution of two matrixes

*********************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#define SIZE 3
//function for printing the matrix
void printingMatrixElements(int rows,int columns,int arr[rows][columns]){
	int i,j;
	
	for(i=0;i<rows;i++){
		for(j=0;j<columns;j++){
			printf("%d\t",arr[i][j]);
		}
		printf("\n");
	}
}

int main(){
	int row,column,i,j,sum=0,k,l;
	srand(getpid());
	printf("Enter row and column values\n");
	scanf("%d %d",&row,&column);
	//declaring the main matrix
	int arr[row][column];
	//declaring the resultant matrix 
	int resultant[row][column];
	//declaring the window matrix
	int window[SIZE][SIZE];
	for(i=0;i<row;i++){
		for(j=0;j<column;j++){
			//generating the main matrix elements randomly
			arr[i][j]=rand()%10;
			resultant[i][j] = 0;
		}
	}
	printf("Displaying the main matrix\n");
	printingMatrixElements(row,column,arr);
	printf("Displaying the resultant matrix before convolution\n");
	printingMatrixElements(row,column,resultant);
	for(i=0;i<SIZE;i++){
		for(j=0;j<SIZE;j++){
			//generating the window matrix elements randomly
			window[i][j]=rand()%10;
		}
	}
	
	printf("Displaying the window matrix\n");
	printingMatrixElements(SIZE,SIZE,window);
	//logic for convolution
	for(i=0;i<row-2;i++){
		for(j=0;j<column-2;j++){
			sum = 0;
			for(k=0;k<=2;k++){
				for(l=0;l<=2;l++){
					sum+=arr[i+k][j+l]*window[k][l];
				}
			}
			resultant[i+1][j+1]=sum;
		}
		
	}
	printf("Displaying the resultant matrix after convolution\n");
	printingMatrixElements(row,column,resultant);
	return 0;
}
/************************************OUTPUT OF THE ABOVE PROGRAM**************************************
Enter row and column values
9 9
Displaying the main matrix
7	6	9	8	6	2	4	8	1	
7	0	3	2	6	1	9	3	1	
7	0	9	9	5	6	5	9	9	
3	7	4	6	4	3	7	4	9	
1	0	9	2	7	9	7	1	8	
8	2	1	1	9	2	1	0	9	
7	5	1	8	0	8	3	6	4	
6	6	0	7	7	1	6	2	0	
7	9	2	5	0	4	9	1	6	
Displaying the resultant matrix before convolution
0	0	0	0	0	0	0	0	0	
0	0	0	0	0	0	0	0	0	
0	0	0	0	0	0	0	0	0	
0	0	0	0	0	0	0	0	0	
0	0	0	0	0	0	0	0	0	
0	0	0	0	0	0	0	0	0	
0	0	0	0	0	0	0	0	0	
0	0	0	0	0	0	0	0	0	
0	0	0	0	0	0	0	0	0	
Displaying the window matrix
1	4	8	
2	2	6	
5	0	6	
Displaying the resultant matrix after convolution
0	0	0	0	0	0	0	0	0	
0	224	178	210	151	169	204	153	0	
0	138	171	169	146	196	176	200	0	
0	182	178	216	179	202	193	272	0	
0	165	117	183	135	197	111	218	0	
0	140	137	136	222	145	127	170	0	
0	84	146	137	158	130	81	145	0	
0	106	202	99	155	162	120	156	0	
0	0	0	0	0	0	0	0	0	
	
*********************************************************************************************************/ 
