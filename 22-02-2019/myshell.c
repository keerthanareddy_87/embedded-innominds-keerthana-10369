/*************************************************************************************************************
NAME                 EMAILID                       EMPLOYEEID                     PHONE NUMBER
--------------------------------------------------------------------------------------------------------------
G KEERTHANAREDDY     keerthanareddy8765@gmail.com        10369                        
9542270284
--------------------------------------------------------------------------------------------------------------
This program is implementation of user defined shell(grep is not implemented)

**************************************************************************************************************/
#include <unistd.h>
#include <sys/types.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
char * arguments[100];


//reading userfrom the stdin
void readComandFromUser() {
	char *string;
	int i=0;
	string=(char *)malloc(20);
	read(0,string,100);
	string[strlen(string)-1]='\0';
	// Returns first token
	char* token = strtok(string, " |");

	while (token != NULL) {
		arguments[i]=token;
		printf("%s\n",arguments[i]);
		i++;
		token = strtok(NULL, " |");
	}
	arguments[i]=token;

}


int main()
{
	int ret = -1,child_exit_status;
	char *userName;
	char cwd[1024];			

	//geting user name
	getlogin_r(userName,10);
	userName[strlen(userName)]='@';
	char name[] = "myShell$ ";
	strcat(userName,name);

	while(1)
	{
		//writing our own username@shellname to the STDOUT
		write(1,userName, strlen(userName));
		readComandFromUser();
		//if entered string equals to PWD
		if(!(strcmp(arguments[0],"pwd"))) {

			getcwd(cwd, sizeof(cwd));
			write(1, cwd, strlen(cwd));
			printf("\n");
			continue;
		}
		//if entered string equals to Exit
		else if(!(strcmp(arguments[0],"exit"))) {

			exit(0);
		}
		//if entered string equals tp cd
		else if(!(strcmp(arguments[0],"cd"))) {

			chdir(arguments[1]);
			getcwd(cwd, sizeof(cwd));
			write(1, cwd, strlen(cwd));
			printf("\n");
			continue;
		}


		//if entered string is any linux command 
		ret = fork();
		if(ret == 0) {

			execvp(arguments[0], arguments);
		}
		else
		{
			wait(&child_exit_status);
		}
	}
	return 0;
}
/***********************************output of the above program***************************************

vidya@myShell$ ls
ls
'\'	    array2.c   b.c     exec.c		      file.c    map.c	    MyShell.c   shellTest.sh   test.c
 a.out	    array.c    bit.c   external_commands.sh   files.c   myshell.c   orphan.c    test1.c        zombie.c
 array1.c   arr.c      exec    file1.c		      fork.c    Myshell.c   programs    test2.c
vidya@myShell$ ls -l
ls
-l
total 124
-rw-r--r--  1 vidya vidya   806 Feb 22 14:38 '\'
-rwxr-xr-x  1 vidya vidya 13128 Feb 22 18:07  a.out
-rw-r--r--  1 vidya vidya   387 Feb  2 11:34  array1.c
-rw-r--r--  1 vidya vidya   509 Feb  2 12:51  array2.c
-rw-r--r--  1 vidya vidya   527 Feb  2 10:40  array.c
-rw-r--r--  1 vidya vidya   280 Feb  2 11:19  arr.c
-rw-r--r--  1 vidya vidya   223 Feb 12 10:25  b.c
-rw-r--r--  1 vidya vidya   123 Feb 13 10:56  bit.c
-rwxr-xr-x  1 vidya vidya  8296 Feb  2 08:44  exec
-rw-r--r--  1 vidya vidya   149 Feb 22 09:38  exec.c
-rwxr-xr-x  1 vidya vidya   619 Feb 22 09:51  external_commands.sh
-rw-r--r--  1 vidya vidya   433 Feb 11 11:26  file1.c
-rw-r--r--  1 vidya vidya   401 Feb 12 10:22  file.c
-rw-r--r--  1 vidya vidya   506 Feb 11 10:44  files.c
-rw-r--r--  1 vidya vidya   134 Feb 22 14:37  fork.c
-rw-r--r--  1 vidya vidya   392 Feb 12 15:54  map.c
-rw-r--r--  1 vidya vidya  1532 Feb 22 18:07  myshell.c
-rw-r--r--  1 vidya vidya  1357 Feb 22 17:47  Myshell.c
-rw-r--r--  1 vidya vidya  2646 Feb 22 17:09  MyShell.c
-rw-r--r--  1 vidya vidya   807 Feb 22 14:43  orphan.c
drwxr-xr-x 15 vidya vidya  4096 Feb 21 08:33  programs
-rwxr-xr-x  1 vidya vidya  1704 Feb 22 09:51  shellTest.sh
-rw-r--r--  1 vidya vidya   370 Feb  6 10:43  test1.c
-rw-r--r--  1 vidya vidya   418 Feb  8 10:58  test2.c
-rw-r--r--  1 vidya vidya   456 Feb  6 09:52  test.c
-rw-r--r--  1 vidya vidya   757 Feb 22 14:45  zombie.c
vidya@myShell$ cd ..
cd
..
/home/vidya
vidya@myShell$ ls
ls
added	     bhaskar_10362  Documents				embedded-innominds-keerthana-10369   examples.desktop  Pictures   Videos
Aspire	     ctraining	    Downloads				embedded-innominds-naidu-10360	     hii.zip	       Public
assignments  Desktop	    embedded-innominds-hemalatha-10374	embedded-innominds-vidyavathi-10358  Music	       Templates
vidya@myShell$ pwd
pwd
/home/vidya
vidya@myShell$ ls -l -ltr
ls
-l
-ltr
total 120
-rw-r--r--  1 vidya vidya  8980 Feb  1 16:08 examples.desktop
drwxr-xr-x  2 vidya vidya  4096 Feb  1 21:59 Videos
drwxr-xr-x  2 vidya vidya  4096 Feb  1 21:59 Templates
drwxr-xr-x  2 vidya vidya  4096 Feb  1 21:59 Public
drwxr-xr-x  2 vidya vidya  4096 Feb  1 21:59 Music
drwxr-xr-x  2 vidya vidya  4096 Feb  1 21:59 Documents
drwxr-xr-x  2 vidya vidya  4096 Feb  4 21:35 assignments
drwxr-xr-x  7 vidya vidya  4096 Feb  5 10:35 Aspire
drwxr-xr-x  9 vidya vidya  4096 Feb 18 22:05 bhaskar_10362
drwxr-xr-x 14 vidya vidya  4096 Feb 19 13:14 embedded-innominds-naidu-10360
drwxr-xr-x 10 vidya vidya  4096 Feb 19 19:30 embedded-innominds-keerthana-10369
drwxr-xr-x 10 vidya vidya  4096 Feb 19 22:07 embedded-innominds-vidyavathi-10358
drwxr-xr-x 10 vidya vidya  4096 Feb 19 22:59 embedded-innominds-hemalatha-10374
drwxr-xr-x  3 vidya vidya  4096 Feb 19 22:59 added
-rw-r--r--  1 vidya vidya 37174 Feb 20 09:26 hii.zip
drwxr-xr-x  2 vidya vidya  4096 Feb 20 14:29 Pictures
drwxr-xr-x  4 vidya vidya  4096 Feb 21 13:38 Desktop
drwxr-xr-x  5 vidya vidya  4096 Feb 22 14:47 Downloads
drwxr-xr-x  4 vidya vidya  4096 Feb 22 18:07 ctraining
vidya@myShell$ exit
exit
************************************************************************************/

