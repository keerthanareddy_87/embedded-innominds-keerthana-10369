//required headers are included
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/*searching the given data in the array by using binary search*/
int binarySearch(int low,int high,int array[],int data){
	int mid;
	//if lower index is greater than the high index then given data is not present in the array so we are return -1
	if(low>high)
		return -1;
	//finding the mid index of the array
	mid=(low+high)/2;
	//if mid value is equal to the given data then returning array index to the called function
	if(array[mid]==data)
		return mid+1;
	//if mid value is greater than the data we are updating the mid value to mid -1 and again calling the binary search function
	if(array[mid]>data){
		mid=mid-1;
		binarySearch(low,mid,array,data);
	}
	/*if mid value is less than the given data  then also we are updating the mid value to mid+1 and again calling the binary search function*/
	else{
		mid=mid+1;
		binarySearch(mid,high,array,data);
	}
}

int main(){
	//initializing array elements
	int array[] = {3,5,7,9,10,14,16,35,67,89,90};
	int data,index,i;
	//finding the size of the array
	int n=sizeof(array)/sizeof(array[0]);
	printf("number of elements in the array are :%d\n",n);
	//printing the array elements 
	printf("elements in the array are:\n");
	for(i=0;i<n;i++){
		printf("%d",array[i]);
		printf("\t");
	}
	printf("\n");
	//entering the data to find in the array
	printf("enter the data to find in the array\n");
	scanf("%d",&data);
	//calling the binary search function to know whether the given data is present in the array are not
	index=binarySearch(0,n-1,array,data);
	//checking whether the data is present in the array are not by checking with the index value
	if(index == -1){
		printf("data not found in the array %d\n",data);
	}else{
		printf("data(%d) found in the array at position :%d\n",data,index);
	}
	//successful termination of the program
	return 0;
}

