#include "server.h"

int serverfd,clientfd,portnum,bindfd;
char buf[1024];
char bye[4] = {'b','y','e','\n'};
int n;

void * serverRead(void *arg){
	//while loop for continues chating
	while(1){
		//clearing the buffer before reading
		memset(buf,0,sizeof(buf));
		//reading the data from the client
		n = read(clientfd,buf,1024);
		if(n < 0){
			perror("ERROR IN READING");
			exit(1);
		}
		//checking the buffer data is equal to bye are not
		if(!(strcmp(buf,bye))){
			printf("client = %s",buf);
			exit(0);
		}
		//printing the data on the output screen
		printf("client =%s",buf);
	}
	return NULL;
}

void * serverWrite(void *arg){
	//while loop for continues chat
	while(1){
		//clearing the buffer before reading
		memset(buf,0,sizeof(buf));
		//reading the data
		fgets(buf,1024,stdin);
		if(n < 0){
			perror("ERROR IN READING");
			exit(1);
		}
		//checking the buffer data is equal to bye are not
		if(!(strcmp(buf,bye))){
			write(clientfd,buf,strlen(buf));
			exit(0);
		}
		//writing to the client
			n=write(clientfd,buf,strlen(buf));
		if(n < 0){
			perror("ERROR IN WRITING");
			exit(1);
		}
	}
	return NULL;
}
//server client continues chat using threads
int main(int argc,char *argv[]){
	//given variable names to the sockaddr structure
	struct sockaddr_in server_addr,client_addr;
	//to store the size of the client structure we are using socklen_t
	socklen_t client_len;
	//thread ids are created
	pthread_t thread_id1,thread_id2;
	//checking for the 2 arguments at command prompt
	if(argc < 2){
		perror("error,port number not provided");
		exit(1);
	}
	//creating socket
	serverfd = socket(AF_INET,SOCK_STREAM,0);
	if(serverfd < 0){
		perror("problem in opening socket");
		exit(1);
	}
	//clearing the server address
	memset((char *)&server_addr,0,sizeof(server_addr));
	//initializing the structure members with the family ,address and port number
	portnum=atoi(argv[1]);
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = INADDR_ANY;
	server_addr.sin_port = htons(portnum);
	//binding the server 
	bindfd = bind(serverfd,(struct sockaddr *)&server_addr,sizeof(server_addr));
	if(bindfd < 0){
		perror("error in binding");
		exit(1);
	}
	//listening for 5 clients
	listen(serverfd,5);
	//finding the length of the client structure
	client_len = sizeof(client_addr);
	//accepting the client
	clientfd=accept(serverfd,(struct sockaddr *)&client_addr,&client_len);
	if(clientfd<0){
		perror("error in accepting ");
		exit(1);
	}
	//creating the threads for server read and write
	pthread_create(&thread_id1,NULL,serverRead ,NULL);
	pthread_create(&thread_id2,NULL,serverWrite ,NULL);
	//joining the threads so that main has to wait 
	pthread_join(thread_id1,NULL);
	pthread_join(thread_id2,NULL);
	//closing the discriptors
	close(serverfd);
	close(clientfd);
}

