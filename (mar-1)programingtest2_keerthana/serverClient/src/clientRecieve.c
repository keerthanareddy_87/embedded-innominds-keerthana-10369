#include "client.h"

int serverfd,portnum;
char buf[1024];
char bye[4] = {'b','y','e','\n'};
int n;

void * clientWrite(void *arg){
	//while loop for continuous chatting
	while(1){
		//clearing the buffer
		memset(buf,0,sizeof(buf));
		//reading data 
		fgets(buf,1024,stdin);
		if(n < 0){
			perror("ERROR IN READING");
			exit(1);
		}
		//comparing the data is equal to bye or not
		if(!(strcmp(buf,bye))){
			write(serverfd,buf,strlen(buf));
			exit(0);
		}
			//writing into the buffer
			n=write(serverfd,buf,strlen(buf));
		if(n < 0){
			perror("ERROR IN WRITING");
			exit(1);
		}
	}
}
void * clientRead(void *arg){
	//while loop for continuous chat
	while(1){
		//clearing buffer
		memset(buf,0,sizeof(buf));
		//reading data from server
		n = read(serverfd,buf,1024);
		if(n < 0){
			perror("ERROR IN READING");
			exit(1);
		}
		//comparing data equal to bye are not
		if(!(strcmp(buf,bye))){
			printf("client = %s",buf);
			exit(0);
		}
		//printing on screen
		printf("client =%s",buf);
	}
}
int main(int argc,char *argv[]){
	//member for structure 
	struct sockaddr_in server_addr;
	//to get the host name
	struct hostent *server;
	//thread ids
	pthread_t thread_id1,thread_id2;
	//checking for 3 arguments
	if(argc < 3){
		perror("error,port number not provided");
		exit(1);
	}
	//creating socket
	serverfd = socket(AF_INET,SOCK_STREAM,0);
	if(serverfd < 0){
		perror("problem in opening socket");
		exit(1);
	}
	//clearing server_addr
	memset((char *)&server_addr,0,sizeof(server_addr));
	//initializing the client data members
	portnum=atoi(argv[2]);
	server = gethostbyname(argv[1]);
	server_addr.sin_family = AF_INET;
	strncpy((char *)server->h_addr,(char *)&server_addr.sin_addr.s_addr,server->h_length);
	server_addr.sin_port = htons(portnum);
	//connect with server
	if(connect(serverfd,(struct sockaddr *)&server_addr,sizeof(server_addr))){
		perror("error in connecting");
		exit(1);
	}
	//creating threads 
	pthread_create(&thread_id1,NULL,clientWrite ,NULL);
	pthread_create(&thread_id2,NULL,clientRead ,NULL);
	//joining the threads
	pthread_join(thread_id1,NULL);
	pthread_join(thread_id2,NULL);
	//closing the server
	close(serverfd);
}

