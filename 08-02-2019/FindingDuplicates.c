/*****************************************************************************************************
 NAME                PHONE NUMBER                      EMAILID                             EMPLOYEE-ID
 -----------------------------------------------------------------------------------------------------------
 G.KEERTHANAREDDY      9542270284                     keerthanareddy8765@gmail.com               10369
 ----------------------------------------------------------------------------------------------------------
 This program is the implementation of finding and the duplicates in the given string

 **********************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(){
	char arr[50]="silence is a source of great strength";
	printf("%s\n",arr);
	for(int i=0;arr[i];i++){
		char temp=arr[i];
		for(int j=i+1;arr[j];j++){
			if(arr[j]==temp){
				memmove(arr+j,arr+j+1,strlen(arr+j+1)+1);
				j--;

			}
		}
	}
	printf("%s\n",arr);
}

/***************************************OUTPUT OF THE PROGRAM**********************************************
silence is a source of great strength
silenc aourfgth
**********************************************************************************************************/
