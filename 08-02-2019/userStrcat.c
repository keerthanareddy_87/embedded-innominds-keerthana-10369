/**********************************************************************************************************************
   NAME                PHONE NUMBER                      EMAILID                             EMPLOYEE-ID
 ---------------------------------------------------------------------------------------------------------------------
  G.KEERTHANAREDDY      9542270284              keerthanareddy8765@gmail.com                   10369
 ---------------------------------------------------------------------------------------------------------------------

 This is the implementation of userdefined function for string concatination

 ********************************************************************************************************************/

#include<stdio.h>
#include<stdlib.h>
//logic for userdefined function for string concatination
char* userStrcat(char *str1,char *str2){
	int i=0,j=0;
	while(str1[i]!='\0'){
		i++;
	}
	while(str1[i]=str2[j]){
		i++;
		j++;
	}
	return str2;

} 
int main(){
	int res;
	char s1[50]="Akshara",s2[50]="....hai";
	printf("**********Before concatination**********\n");
	printf("string 1 = %s\n",s1);
	printf("string 2 = %s\n",s2);
	userStrcat(s1,s2);
	printf("***********After Cancatination**********\n");
	printf("string 1=%s\n",s1);
}

/***********************************************OUTPUT OF THE PROGRAM******************************************
**********Before concatination**********
string 1 = Akshara
string 2 = ....hai
***********After Cancatination**********
string 1=Akshara....hai
**************************************************************************************************************/
