/*****************************************************************************************************
 NAME                PHONE NUMBER                      EMAILID                             EMPLOYEE-ID
 -----------------------------------------------------------------------------------------------------------
 G.KEERTHANAREDDY    9542270284                     keerthanareddy8765@gmail.com               10369
 ----------------------------------------------------------------------------------------------------------
  This program is the implementation of reversing the sentence and words

 *******************************************************************************************************/



#include<stdio.h>
#include<string.h>
main() {
	//declaring the character array and temp variable
        char array[80],temp;
        int l,i,j,a,b;
        printf("enter the string:");
        gets(array);
        l=strlen(array)-1;
	//logic for reversing the string
        for(i=0,j=l;i<j;i++,j--) {
                temp=array[i];
                array[i]=array[j];
                array[j]=temp;
        }
	//logic for reversing the words
        for(i=0;array[i];i++,a=0,b=0) {
                a=i;
                while(array[i]!=32) {
                        if(array[i]==0)
                                break;
                        b=i;
                        i++;
                }
                for(;a<b;a++,b--) {
                        temp=array[a];
                        array[a]=array[b];
                        array[b]=temp;
                }
        }
        array[i]=0;
        puts(array);
}

/********************************************OUTPUT OF THE ABOVE PROGRAM********************************
enter the string:my name is keerthana
keerthana is name my
*******************************************************************************************************/
 
