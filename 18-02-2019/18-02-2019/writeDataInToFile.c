/***************************************************************************************************************
NAME                      EMAILID                            PHONE                   EMPID
------------------------------------------------------------------------------------------------------------
G KEERTHANA REDDY        keerthanareddy8765@gmail.com     9542270284                10369

PURPOSE:
----------------
This program is to write content to the console ,reading data from the console and writing data in to another file
*******************************************************************************************************/

#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>

int main() {

	int fp1,size;
	char buff[100];
	fp1=open("file.txt",O_WRONLY | O_CREAT);
	if (fp1 == -1)

		printf("Error in opening the file  \n"); 
	else 
			printf("input file opened in read mode\n");
	printf("write the conent that should be written into file\n");
	printf("..............press (CNTRL+c) to exit the loop .......\n");
	while(1) {
	size=read(0,buff,sizeof(buff));
	//writing from input file to output file
	write(fp1,buff,size);
	}
	printf("writing to output file using system call successfull\n");
	close(fp1);
	return 0;

}

/********************OUTPUT****************************************
input file opened in read mode
write the conent that should be written into file
..............press (CNTRL+c) to exit the loop .......
my name is keerthana 
i am a job holder in innominds
^C
---------------------------------------------------
$ cat file.txt 
my name is keerthana 
i am a job holder in innominds
**********************************************************************/

