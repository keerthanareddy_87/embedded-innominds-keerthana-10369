/***************************************************************************************
NAME                      EMAILID                            PHONE                   EMPID
------------------------------------------------------------------------------------------------------------
G KEERTHANA REDDY        keerthanareddy8765@gmail.com     9542270284                10369

PURPOSE:
----------------
This program is to display the all status of the file,where file name is entered through command line argumnent
*****************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>
#include<time.h>


int main(int argc ,char **argv){
	struct stat v;
	if(stat(argv[1],&v)==-1){
		printf(" there is no such file or invalid input....give correct file name in command line\n");
		return 0;
	}
	printf("size=%d\n",(int)v.st_size);
	printf("link count=%d\n",(int)v.st_nlink);
	printf("inode Number=%d\n",(int)v.st_ino);
//to check the permission of the file 
	printf("FILE PERMISSIONS:\n");
	if((v.st_mode>>8)&1)
	printf("r");
	else 
		printf("-");

	if((v.st_mode>>7)&1)
	printf("w");
	else 
		printf("-");
	if((v.st_mode>>6)&1)
	printf("x");
	else 
		printf("-");
	if((v.st_mode>>5)&1)
	printf("r");
	else 
		printf("-");
	if((v.st_mode>>4)&1)
	printf("w");
	else 
		printf("-");
	if((v.st_mode>>3)&1)
	printf("x");
	else 
		printf("-");

	if((v.st_mode>>2)&1)
	printf("r");
	else 
		printf("-");
	if((v.st_mode>>1)&1)
	printf("w");
	else 
		printf("-");
	if((v.st_mode>>0)&1)
	printf("x");
	else 
		printf("-");
	printf("\n");
	if((v.st_mode>>15)&1){
		printf("regular file\n");
	}
	else if(((v.st_mode>>15)&1)&&((v.st_mode>>14)&1)){
		printf("socket file\n");
	}
	else if(((v.st_mode>>15)&1)&&((v.st_mode>>13)&1)){
		printf("linkfile\n");
	}
	else{
		printf("character file\n");
	}
	printf("uid:%u\n",v.st_uid);
	printf("gid:%u\n",v.st_gid);
	printf("mode:%d\n",v.st_mode);
	printf("blocks:%ld\n",v.st_blocks);
	printf("IO-blocks:%ld\n",v.st_blksize);
	
	printf("_--------------_-_-_____------------\n");

	printf("Access time = %s\n",ctime(&v.st_atime));
	printf("Modification time = %s\n",ctime(&v.st_mtime));
	printf("change time = %s\n",ctime(&v.st_mtime));
		
			
	return 0;
			
}

/*****************************OUTPUT***********************************

$ ./a.out Fibonacci.c
size=1448
link count=1
inode Number=2891231
FILE PERMISSIONS:
rw-r--r--
regular file
uid:1000
gid:1000
mode:33188
blocks:8
IO-blocks:4096
_--------------_-_-_____------------
Access time = Tue Feb 19 19:04:42 2019

Modification time = Tue Feb 19 19:04:42 2019

change time = Tue Feb 19 19:04:42 2019


************************************************************************/
