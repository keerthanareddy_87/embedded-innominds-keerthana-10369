/*---------------------------------------------------------------------------------------------------------------------------------*/
/*                 NAME:                 EMPLOYEE-ID:           PHONE_NUMBER:                          GMAIL-ID:

              KEERTHANA REDDY          10369                   9542270284                      keerthanareddy8765@gmail.com
   PURPOSE:
 ---------
 This program  will display the number of equilateral triangles in the given matrix  

------------------------------------------------------------------------------------------------------------------------------------*/


#include<cstdio>
#include<cstdlib>
#include<unistd.h>
#include<iostream>
#include<vector>
using namespace std;
//creating the Mytriangle class
class Triangle_Equilateral {
		//declaring the data members
		int row,column,i,j,cnt;
		vector<vector<int> > matrix;
	public:
		//default constrctor for Mytriangle class
		Triangle_Equilateral() {
			srand(getpid());
			row=random()%10;
			column=random()%10;
			
			for(int i = 0; i < row; ++i) {
			matrix.push_back(vector<int>());
				for(j=0;j<column;j++)
					matrix[i].push_back(random()%10);
			}
		}
		//member function printMatrix for printing the matrix
		void printMatrix() {
			for(i=0;i<row;i++,cout<<endl) {
				for(j=0;j<column;j++) {
					cout<<matrix[i][j]<<" ";
				}
			}
		}
		//member function findTriangles to find out the triangles present in given matrix
		void findTriangles() {
			int n;
			cnt=0;
			for(n=1;n<=row-1;n++) {

				for(i=0;i<row-n;i++) {
					for(j=n;j<column-n;j++) {

						if(matrix[i][j]>0) {

							if(matrix[i+n][j-n]>0 && matrix[i+n][j+n]>0) {
								cnt++;
							}

						}

					}

				}
			}

			cout<<"no.of equailateral triangles in a matrix:"<<cnt<<endl;
		}

};
//main method
int main(void) {
	Triangle_Equilateral triangle;
	triangle.printMatrix();
	triangle.findTriangles();
	return 0;
}




/******************************************************************************************************************
   ---------OUTPUT---------

3 6 4 6 2 0 2 
1 7 3 0 6 8 5 
3 8 1 1 7 6 5 
6 2 3 0 8 3 8 
8 3 7 1 9 3 9 
3 3 4 7 3 7 9 
9 7 4 3 5 7 6 
3 4 3 1 8 7 3 
no.of equailateral triangles in a matrix:47

********************************************************************************************************************/
