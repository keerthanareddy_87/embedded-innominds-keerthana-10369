/*************************************************************************************************************************
 NAME                           EMAIL-ID                        PHONE NUMBER                    EMPLOYEE-ID
 -------------------------------------------------------------------------------------------------------------------------
 G KEERTHANA REDDY       keerthanareddy8765@gmail.com           9542270284                        10369
 -------------------------------------------------------------------------------------------------------------------------
 This program is the implementation of checking the last set bit.
**************************************************************************************************************************/

#include<stdio.h>
int main(){
	int data;
	printf("enter the data\n");
	scanf("%d",&data);
	for(int i=0;i<=31;i++){
		if((data>>i)&1){
			printf("%d\n",i+1);
			break;
		}

	}
	return 0;
}
/************************OUTPUT**************************************
enter the data
5
1
-----------------------------------------
enter the data
9
1
-----------------------------------------
enter the data
3
1
-----------------------------------------
enter the data
4
3
******************************************************************/
