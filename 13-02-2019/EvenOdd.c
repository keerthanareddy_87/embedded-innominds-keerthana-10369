/*************************************************************************************************************************
 NAME                           EMAIL-ID                        PHONE NUMBER                    EMPLOYEE-ID
 -------------------------------------------------------------------------------------------------------------------------
 G KEERTHANA REDDY         keerthanareddy8765@gmail.com         9542270284                      10369
 -------------------------------------------------------------------------------------------------------------------------
 This program is the implementation checking given number is even or odd.
**************************************************************************************************************************/

#include<stdio.h>

int main(){
	//declaring the variables
	int data;
	printf("Enter the data\n");
	scanf("%d",&data);
	//condition for even or odd
	if(data&1){
		printf("odd number\n");
	}else{
		printf("Even number\n");
	}
	return 0;
}
/***************************OUTPUT FOR THE ABOVE PROGRAM***********************************
Enter the data
64
Even number
******************************************************************************************/
