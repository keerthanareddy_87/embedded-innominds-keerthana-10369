/*pair program*/
/*----------------------------------------------------------------------------------------------------------------------*/
/*              NAME:                 EMPLOYEE-ID:           PHONE_NUMBER:                          GMAIL-ID:

               KEERTHANA REDDY          10369                   9542270284                      keerthanareddy8765@gmail.com
   PURPOSE:
 ---------

 In this client program we are using the concept of shared memory to share the data between the client and server and 
 creating three threads , signaling the threads by using semaphore.    
*-----------------------------------------------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <iostream> 
#include <sys/ipc.h> 
#include <sys/shm.h>
#include <unistd.h>
#include<semaphore.h>
using namespace std;


	sem_t mutex;
//Client class which contains data member and member functions 
class ClientToReadRespond{
	//Data members
	int shmid;
	pthread_t thread_id[3];
	char *name;
	public :
	//default constructor 
	ClientToReadRespond() {

		key_t key = ftok("myfile1",85);
		shmid = shmget(key,1024,0666|IPC_CREAT); 


		char  *str = (char*) shmat(shmid,(void*)0,0); 

		cout<<str<<endl;
		shmdt(str);
	}
	
	//This method used for creating threads
	void Thread_Creation(int i){
		cout<<"enter client name :"<<endl;
		name=(char*) shmat(shmid,(void*)0,0); 
		cin>>name;

		pthread_create( &thread_id[i], NULL,display_Client, (void*) name);
	}

	//display_Client details
	static void *display_Client( void *ptr )
	{
  	 	char *name;
                name = (char *) ptr;
                printf("my name is %s \n", name);

	}
	//method for joining threads
	void joiningThread(int j) {


		pthread_join( thread_id[j], NULL); 
	}


};



int main()
{
	ClientToReadRespond clientObj;
	sem_init(&mutex, 0, 1); 
	int i,j;
	//For loop for creating 3 threads
	for(i=0; i < 3; i++)
	{

		sem_wait(&mutex); 
		clientObj.Thread_Creation(i);

		sleep(10);
		printf("\nJust Exiting...\n"); 
		sem_post(&mutex); 
		
	}
	//for loop for joining 3 threads
	for(j=0; j < 3; j++)
	{

		clientObj.joiningThread(j);
	}
	sem_destroy(&mutex); 
	return 0; 

}



/*********************************************************************************************************
      
     OUTPUT
--------------

 hello
enter client name :
keerthana
my name is keerthana 

Just Exiting...
enter client name :
avinash
my name is avinash 

Just Exiting...
enter client name :
prasanna
my name is prasanna 

Just Exiting...


**********************************************************************************************************/
