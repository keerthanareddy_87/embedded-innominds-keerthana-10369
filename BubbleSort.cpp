#include<iostream>
/*---------------------------------------------------------------------------------------------------------------------------------*/
/*              NAME:                 EMPLOYEE-ID:           PHONE_NUMBER:                          GMAIL-ID:

            KEERTHANA REDDY          10369                   9542270284                      keerthanareddy8765@gmail.com
   PURPOSE:
 ---------

This program sort the array elements(dynamically allocated) by using BUBBLE SORT algorithm


------------------------------------------------------------------------------------------------------------------------------------*/
#include<iostream>
using namespace std;

/*This class contain data members and member functions like bubblesort and displayarrayelements to sort the array elements*/ 
class BubbleSort{

        int *array;
        int size;
        public:
		/*Default constructor*/
                BubbleSort(){
                        cout<<"enter the size of array"<<endl;
                        cin>>size;
                        array = new int[size];
                        cout<<"enter array elements:"<<endl;
                        for(int i=0;i<size;i++)
                        cin>>array[i];
                        }
                void bubbleSort();
                void displayArrayElements();
		/*Default destructor*/
                ~BubbleSort(){
                }
};

/*bubble sort to sort the array elements*/
void BubbleSort :: bubbleSort(){
	int temp;
	for(int i=0;i<size-1;i++){
		for(int j=0;j<size-i-1;j++){
			if(array[j]>array[j+1]){
				temp=array[j+1];
				array[j+1]=array[j];
				array[j]=temp;
			}
		}
	}
}
/*display array elements*/
void BubbleSort :: displayArrayElements(){
        for(int i=0;i<size;i++){
        cout<<array[i]<<" ";
	}

}

int main(){
	
	/*creating object */
	BubbleSort bubblesort;
	cout<<"Array elements Before sorting:"<<endl;
	bubblesort.displayArrayElements();
	bubblesort.bubbleSort();
	cout<<endl<<"Array elements After sorting:"<<endl;	
	bubblesort.displayArrayElements();
	return 0;
}


/*-----------------------------------------------------------------------------------------------------------------

	OUTPUT:

--------------------------
enter the size of array
7
enter array elements:
4
1
65
9
35
8
21
Array elements Before sorting:
4 1 65 9 35 8 21 
Array elements After sorting:
1 4 8 9 21 35 65 


---------------------------------------------------------------------------------------------------------------------*/
