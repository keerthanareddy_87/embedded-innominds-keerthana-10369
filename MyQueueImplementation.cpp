/*---------------------------------------------------------------------------------------------------------------------------------*/
/*    BATCH:              NAME:                 EMPLOYEE-ID:           PHONE_NUMBER:                          GMAIL-ID:

      GROUP-B          KEERTHANA REDDY          10369                   9542270284                      keerthanareddy8765@gmail.com
   PURPOSE:
 ---------
In this program Queue we are adding data into the queue from rare end and delete the data from front end  by using methods 
like enque and deque


--------------------------------------------------------------------------------------------------------------------------------------*/
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
using namespace std;

/*In This class we used structure to hold the data and self referential pointer to point to the next instruction and methods like
enque and deque*/  
class MyQueueImplementation {

	struct queue {
		string name;		
		struct queue *next;
	}*front,*rare;
   public:
	//Default construtor
	MyQueueImplementation() {
		front=NULL;
		rare=NULL;
	}
	//adding data into the queue
	void enque(string name) {
		struct queue *new_record=new struct queue;
		new_record->name=name;
		if(front==NULL) {
			front=new_record;
			rare=new_record;
			new_record->next=NULL;
			return;
		}
		rare->next=new_record;
		rare=new_record;
		return;
	}
	//deleting the data from the queue
	string deque() {
		string data;
		if(front==NULL) {
			cout<<"Queue is empty"<<endl;
			return NULL;
		}
		struct queue *temp=front;
		data=temp->name;
		front=front->next;
		delete temp;
		temp=NULL;
		return data;
	}
	//displaying the data present in the queue
	void displayQueue() {
		struct queue *temp=front;
		cout<<"*****records in Queue:*****"<<endl;
		for(;temp!=NULL;temp=temp->next) {
			cout<<temp->name<<endl;
		}
	}

};

int main(void) {
	//creating object 
	MyQueueImplementation myqueueimplementation;
	int choice;
	string name;
	while(1) {
		cout<<"*****************************MENU************************"<<endl;
		cout<<"1.Enque(adding data into the queue)"<<endl;
		cout<<"2.Deque(deleting the data from the queue)"<<endl;
		cout<<"3.display records present in queue"<<endl;
		cout<<"4.quit"<<endl;
		cout<<"enter your choice:"<<endl;
		cin>>choice;
		switch(choice) {
			case 1:
				cout<<"enter name:"<<endl;
				cin>>name;
				myqueueimplementation.enque(name);
				break;
			case 2:
				cout<<"record with data:"<<myqueueimplementation.deque()<<" was deleted"<<endl;
				break;
			case 3:
				myqueueimplementation.displayQueue();
				break;
			case 4:
				exit(0);
			default:
				cout<<"invalid choice"<<endl;
		}
	} 


}


/***********************************************************************************************************


	OUTPUT:
-----------------------------
*****************************MENU************************
1.Enque(adding data into the queue)
2.Deque(deleting the data from the queue)
3.display records present in queue
4.quit
enter your choice:
1
enter name:
keerthana
*****************************MENU************************
1.Enque(adding data into the queue)
2.Deque(deleting the data from the queue)
3.display records present in queue
4.quit
enter your choice:
1
enter name:
bhavani
*****************************MENU************************
1.Enque(adding data into the queue)
2.Deque(deleting the data from the queue)
3.display records present in queue
4.quit
enter your choice:
1
enter name:
madhu
*****************************MENU************************
1.Enque(adding data into the queue)
2.Deque(deleting the data from the queue)
3.display records present in queue
4.quit
enter your choice:
3
*****records in Queue:*****
keerthana
bhavani
madhu
*****************************MENU************************
1.Enque(adding data into the queue)
2.Deque(deleting the data from the queue)
3.display records present in queue
4.quit
enter your choice:
2
record with data:keerthana was deleted
*****************************MENU************************
1.Enque(adding data into the queue)
2.Deque(deleting the data from the queue)
3.display records present in queue
4.quit
enter your choice:
3
*****records in Queue:*****
bhavani
madhu
*******************************************************************************************************************/
