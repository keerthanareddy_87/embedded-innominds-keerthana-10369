/***************************************************************************************
NAME                      EMAILID                            PHONE                   EMPID
------------------------------------------------------------------------------------------------------------
G KEERTHANA REDDY        keerthanareddy8765@gmail.com     9542270284                10369

PURPOSE:
----------------
This program is to find fibonacci series upto given number using recursion
*****************************************************************************************/
#include<stdio.h> 
//this function finds the fibonacci series for the given number
int Fibonacci(int n)
{
	if ( n == 0 )
		return 0;
	else if ( n == 1 )
		return 1;
	else
		return ( Fibonacci(n-1) + Fibonacci(n-2) );
} 

int main () 
{ 

	int num,i=0;
	printf("enter the number to find the fibonacci series :\n");
	scanf("%d",&num);
	while(num<0) {
		printf("fibonacci number  cannot be negative......re enter \n");
		scanf("%d",&num);
	}

	printf("Fibonacci series for the given number is \n");

	for (int j = 1 ; j <= num ; j++ )
	{
		printf("%d ", Fibonacci(i));
		i++; 
	}
	printf("\n");

	return 0;
}

/*********************************OUTPUT*************************************
enter the number to find the fibonacci series :
8
Fibonacci series for the given number is 
0 1 1 2 3 5 8 13 
*******************************************************************************/
