/************************************************************************************************************
    NAME 		MOBILE NUMBER 			EMAIL-ID 			EMPLOYEE-ID
------------------------------------------------------------------------------------------------------------
G.KEERTHANAREDDY        9542270284                      keerthanareddy8765@gmail.com     10369
************************************************************************************************************

This program is to check whether the given integer is palindrome or not without reversing the given data.

*************************************************************************************************************/



#include<stdio.h>
#include<math.h>

//method to count the number of digits in integer
int countDigits(int data){
	int noOfDigits=0,temp=data;
	while(temp%10||temp){
		noOfDigits++;
		temp/=10;
	}
	printf("no of digits=%d\n",noOfDigits);
	return noOfDigits;
}

//method to check whether the given number is palindrome or not without reversing the integer  
int isPalindrome(int data){
	int temp=data,noOfDigits;
	int firstterm,secondterm,a,b;
	noOfDigits=countDigits(data);
	a=temp%10;
	b=temp/pow(10,noOfDigits-1);
	//checking whether the first and last digit is same or not
	while(a==b){
		secondterm=pow(10,noOfDigits-1);
		firstterm=temp%secondterm;
		temp=firstterm/10;
		noOfDigits-=2;
		if(noOfDigits == 1||noOfDigits==0){
			return 1;
		}
		a=temp%10;
		b=temp/pow(10,noOfDigits-1);
	}
	return 0;
}
int main(){
	int data,flag=0;
	printf("enter data to check whether the number is palindrome or not\n");
	scanf("%d",&data);
	flag=isPalindrome(data);
	if(flag){
		printf("given data is palindrome number\n");
	}else{
		printf("given data is not palindrome number\n");
	}
	return 0;

}


/*******************************OUTPUT******************************************
enter data to check whether the number is palindrome or not
21486214123
no of digits=8
given data is not palindrome number
-------------------------------------------------------------------------------
enter data to check whether the number is palindrome or not
987656789
no of digits=9
given data is palindrome number
*******************************************************************************/
