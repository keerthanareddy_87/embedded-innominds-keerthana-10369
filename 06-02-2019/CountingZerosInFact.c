/************************************************************************************************************
    NAME                MOBILE NUMBER                   EMAIL-ID                        EMPLOYEE-ID
------------------------------------------------------------------------------------------------------------
G.KEERTHANAREDDY        9542270284                      keerthanareddy8765@gmail.com     10369
***********************************************************************************************************

This program is to count the number of zeros in the last after finding the factorial of a given number.

*************************************************************************************************************/

#include<stdio.h>
#include<math.h>
int main(){
	int data,numberOfZeros = 0,c = 1;
	printf("Enter data to find number of zeros\n");
	scanf("%d",&data);
	while(data>=pow(5,c)){
		numberOfZeros = numberOfZeros+(data/pow(5,c));
		c++;
	}
	printf("Number of zeros in the given data is %d\n",numberOfZeros);
}


/****************************************OUTPUT***********************************************************
Enter data to find number of zeros
45
Number of zeros in the given data is 10
---------------------------------------------------------------------------------------------------------
Enter data to find number of zeros
87
Number of zeros in the given data is 20
---------------------------------------------------------------------------------------------------------
Enter data to find number of zeros
5
Number of zeros in the given data is 1
*********************************************************************************************************/
